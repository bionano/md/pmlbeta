Commands
========

The following commands are defined by the `pmlbeta` plugin:

α/β-peptide Construction and Manipulation
-----------------------------------------

:ref:`betafab2_manpage`
    build a beta-peptide
:ref:`fold_bp_manpage`
    fold a beta-peptide into the desired secondary structure
:ref:`select_bbb_manpage`
    select the beta-backbone

Residue Topology Recognition
----------------------------

:ref:`renumber_peptide_chain_manpage`
    recognize and mark residues in a peptide chain
:ref:`residue_topology_candidates_manpage`
    find matching building block topologies from a GROMACS residue topology database file
:ref:`assign_residue_topology_manpage`
    assign atom properties from a matching building block topology

Input / Output
--------------

:ref:`save_crd_manpage`
    output to the extended CHARMM CRD format
:ref:`save_gro_manpage`
    output to the GRO format of GROMACS
:ref:`save_g96_manpage`
    output to the G96 format used by GROMOS and GROMACS

.. _ssdb_commands:

Secondary structure database manipulation
-----------------------------------------

:ref:`ssdb_add_manpage`
    add/edit an entry
:ref:`ssdb_del_manpage`
    remove an entry
:ref:`ssdb_list_manpage`
    list the entries
:ref:`ssdb_dihedrals_manpage`
    get the dihedral angles corresponding to an entry
:ref:`ssdb_resetdefaults_manpage`
    reset the secondary structure database to its default state

Miscellaneous
-------------
:ref:`gmx_beta_backbone_dihedrals_selection_manpage`
    create selection files for beta-peptide backbone dihedrals, understandabe by GROMACS

.. toctree::
    :hidden:

    betafab2
    fold_bp
    select_bbb
    save_crd
    save_g96
    save_gro
    ssdb_add
    ssdb_del
    ssdb_list
    ssdb_dihedrals
    ssdb_resetdefaults
    gmx_beta_backbone_dihedrals_selection
    renumber_peptide_chain
    residue_topology_candidates
    assign_residue_topology
