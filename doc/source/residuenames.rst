β-amino acid residue naming
===========================

General nomenclature
--------------------

Acyclic β-amino acids come in four varieties, according to the substitution site (see the figure)

β\ :sup:`2`
    Monosubstituted β-amino acid, the side-chain is on the α-carbon.

β\ :sup:`3`
    Monosubstituted β-amino acid, the side-chain is on the β-carbon.

β\ :sup:`2,3`
    Disubstituted β-amino acid, one side-chain on both the α- and the β-carbon.

β-Alanine
    A simple β-backbone, without side-chains

.. figure:: _static/diamides.png
    :figwidth: 70%
    :align: center

    The four kinds of β-amino acids: bare β-backbone or β-alanine (a),
    β\ :sup:`2`\ -amino acid (b), β\ :sup:`3`\ -amino acid (c) and β\ :sup:`2,3`\ -amino acid (d)


In contrast to their natural counterparts, β-amino acids do not have a single,
generally accepted nomenclature. We adopt here the following, widely used convention, which
emphasizes the homology with α-amino acids and allows to account for the absolute
conformation (chirality) as well. The general block format is :

``<chirality><substitution type>h<side-chain designation>``, where:

<chirality>
    is the designation of the chirality of the substitution site atoms. For monosubstituted β-amino acids, this is either
    (\ *S*\ ) or (\ *R*\ ), including parentheses. For disubstituted ones, the substitution site is also labelled
    to avoid ambiguity, i.e. (2\ *S*, 3\ *R*) etc.

<substitution type>:
    either β\ :sup:`2`\ , β\ :sup:`3`\ or β\ :sup:`2,3`\ .

<side-chain designation>:
    single-letter abbreviation of the proteinogenic amino-acid whose side-chain is referred to. As for the chirality above,
    in the case of disubstituted amino-acids the substitution site must be explicitly given in order to avoid
    confusion, i.e. (2A,3Q), etc.

We include α-amino acids in this notation, too, with the following scheme:
``<chirality>α<side-chain designation>``, where:

<chirality>
    is similar as for β\ :sup:`2`\ - or β\ :sup:`3`\ -amino acids, i.e. either
    (\ *S*\ ) or (\ *R*\ ). Additionally, for *D* and *L* are also supported for convenience [1]_.

<side-chain designation>:
    is once again the single-letter abbreviation of proteinogenic amino acids

Examples:
    monosubstituted:
        * (\ *S*\ )β\ :sup:`2`\ hV: valine side-chain on the α-carbon with *S* chirality
        * (\ *R*\ )β\ :sup:`3`\ hR: arginine side-chain on the β-carbon with *R* chirality

    disubstituted:
        * (2\ *S*\ ,3\ *R*\ )β\ :sup:`2,3` h(2A,3L): disubstituted β-amino acid with an alanine side-chain on the α-carbon
          (with *S* chirality) and an arginine on the β-carbon (*R* chirality)

    achiral (bare backbone):
        * βA

    α-amino acids:
        * (\ *S*\ )αV: L-valine
        * (\ *R*\ )αW: D-tryptophan

Two, more complicated examples are shown in the next two figures:

.. figure:: _static/hairpin6.png
    :figwidth: 70%
    :align: center

    (2\ *R*\ ,3\ *S*\ )β\ :sup:`2,3`\ h(2A,3A) -
    (2\ *R*\ ,3\ *S*\ )β\ :sup:`2,3`\ h(2A,3V) -
    (\ *S*\ )β\ :sup:`2`\ hV -
    (\ *S*\ )β\ :sup:`3`\ hK -
    (2\ *R*\ ,3\ *S*\ )β\ :sup:`2,3`\ h(2A,3A) -
    (2\ *R*\ ,3\ *S*\ )β\ :sup:`2,3`\ h(2A,3L)

.. figure:: _static/valxval.png
    :figwidth: 70%
    :align: center

    (\ *S*\ )β\ :sup:`3`\ hV -
    (\ *S*\ )β\ :sup:`3`\ hA -
    (\ *S*\ )β\ :sup:`3`\ hL -
    (2\ *S*\ ,3\ *S*\ )β\ :sup:`2,3`\ h(2A,3A) -
    (\ *S*\ )β\ :sup:`3`\ hV -
    (\ *S*\ )β\ :sup:`3`\ hA -
    (\ *S*\ )β\ :sup:`3`\ hL

.. _simplified_residue_naming:

Simplified nomenclature
-----------------------

In the :ref:`betafab2_manpage` command, essentially the above defined notation is used for defining a β-peptide sequence,
with the following simplification:

    * superscripts are omitted
    * instead of the greek letter β the capital "B" is used
    * commas in amino-acid abbreviations are omitted, they are
      used instead for separating the subsequent residues in the
      peptide chain
    * letters *S* and *R*, describing the absolute conformation, are not italicized

For example, to construct the above two peptides, the following PyMOL commands can be used:

.. code::

    betafab2 hp6, (2R3S)B23h(2A3A), (2R3S)B23h(2A3V), (S)B2hV, (S)B3hK, (2R3S)B23h(2A3A), (2R3S)B23h(2A3L)

    betafab2 valxval, (S)B3hV, (S)B3hA, (S)B3hL, (2S3S)B23h(2A3A), (S)B3hV, (S)B3hA, (S)B3hL

In addition, α-amino acids are also supported with the following notation:

.. code::

    betafab valval, (S)AV, (S)AA, (L)AL, (D)AV, (S)AA, (S)AL

    betafab mixed, (S)AV, (S)B3hV, (R)B2hA, (S)AQ, (2R3S)B23h(2C3W)

Additionally, we now support the two most common cyclic beta-residues: 2-aminocyclopentanecarboxylic acid (ACPC) and
2-aminocyclohexanecarboxylic acid (ACHC) with the syntax:

.. code::

    betafab transachc, (2S3R)ACHC
    betafab cisacpc, (2R3S)ACPC

Capping groups can also be added:

    N-terminal:
        - ACE (acetyl)
        - BUT (butyryl)

    C-terminal:
        - NME (N-methylamide)

.. code::

    betafab ace_valxal_nme, ACE, (S)B3hV, (S)B3hA, (S)B3hL, (2S3S)B23h(2A3A), (S)B3hV, (S)B3hA, (S)B3hL, NME


Secondary structure
-------------------

In addition to designating the amino-acids, the desired fold can also be given in the above notation. Each residue can
be followed by a secondary structure descriptor in one of the following forms:

    #. A square bracket-enclosed, space-separated triplet (pair) of floating point numbers, e.g.
       `(S)AQ[-57 -47]` or `(S)B3hA[-140.3 66.5 -136.8]`

    #. The name of an entry in the :ref:`secondary structure database <secstruct_db>`, enclosed in curly braces, e.g.
       `(S)AQ{Alpha-helix}` or `(S)B3hA{H14M}`

.. toctree::
    singleletter

.. [1] Note that because for β-amino acids no internationally agreed convention exists on the *D* / *L* nomenclature,
    their chirality can only be specified using the unambiguous *S* / *R* notation.
