.. _bugreport:

Reporting bugs
==============

If you find bugs, errors or other unintentional/clumsy behaviour of the program, please file a bug report at:

https://gitlab.com/awacha/pmlbeta/issues/new

Before filing the report, please take care that you use the most up-to-date version:
the bug might have already been fixed.

A helpful bug report should include:
    - Operating system
    - PyMOL version
    - Python version
    - Description of the error
    - Expected behaviour
    - Detailed steps on how to reproduce the bug
    - If applicable, the error messages printed by PyMOL



