.. _citing:

Citing pmlbeta
==============

If you find this plugin useful in your research work and publish a paper, the authors would be grateful if you could cite the corresponding article:

Wacha, András, and Tamás Beke-Somfai. 2021. “PmlBeta: A PyMOL Extension for Building β-Amino Acid Insertions and β-Peptide Sequences.” SoftwareX 13 (January): 100654. https://doi.org/10.1016/j.softx.2020.100654.

