Residue Topology Recognition
============================

Another useful functionality of `pmlbeta` is residue topology recognition. For actual work with molecular models an
accurate designation of various portions of the molecule ("residues", e.g. amino-acids or other building blocks) and
atom naming is often required. While the models constructed by `betafab` are labeled according to the nomenclature of
the CHARMM force field, re-labeling for a different molecular mechanics force field or labeling an unlabeled molecule
(e.g. obtained from crystallography or NMR experiments) is sometimes needed. This package has the following methods:

- Given the N-terminal nitrogen and the C-terminal carbon atoms, command :ref:`renumber_peptide_chain_manpage` finds and
  marks the residues in a peptide chain (either α or β).

- Command :ref:`residue_topology_candidates_manpage` finds matching building block topologies from a GROMACS residue
  topology database file for each residue in the molecule using graph isomorphishm.

- Command :ref:`assign_residue_topology_manpage` assigns atom properties (name, residue name, partial charge) from a
  corresponding building block topology in a GROMACS topology database

- The functionality of the above two commands is implemented in a user-friendly graphical tool accessible from the menu.
