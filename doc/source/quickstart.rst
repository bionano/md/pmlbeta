Quick start
===========

The main goal of `pmlbeta` is to provide tools for building molecular models
of β-peptides (or even mixed α/β-peptides). The idea is mostly taken from the
`fab` command of PyMOL, which can create peptides in a desired secondary
structure. The construction of β-peptides can be done by two means: either
using the command-line or a graphical user interface.

Command line
------------

The command :ref:`betafab2_manpage` can be used for creating α/β-peptides
in extended conformation:

.. code::

    betafab2 hp6, (2R3S)B23h(2A3A), (2R3S)B23h(2A3V), (S)B2hV, (S)B3hK, (2R3S)B23h(2A3A), (2R3S)B23h(2A3L)

    betafab2 valxval, (S)B3hV, (S)B3hA, (S)B3hL, (2S3S)B23h(2A3A), (S)B3hV, (S)B3hA, (S)B3hL

    betafab valval, (S)AV, (S)AA, (S)AL, (S)AV, (S)AA, (S)AL

    betafab mixed, (S)AV, (S)B3hV, (R)B2hA, (S)AQ, (2R3S)B23h(2C3W)

The first argument is always the PyMOL object name, followed by the residue abbreviations following the notation in
:ref:`simplified_residue_naming`

Folding a peptide into the desired secondary structure is possible by supplying the desired secondary structures for
the amino-acids in `betafab2`:

.. code::

    betafab2 valxval, (S)B3hV{H14M}, (S)B3hA[-140.3 66.5 136.8], (S)B3hL{H14M}

Another option is to fold an already built peptide using the :ref:`fold_bp_manpage` command:

.. code::

    fold_bp H14M, valxval   # folds all residues the "valxval" model to the H14M helix

    fold_bp (-140.3 66.5 136.8), valxval    # the same as above, just the torsion angles are explicitly given

Instead of folding the entire peptide to the same secondary structure, this can be done residue-by-residue:

.. code::

    betafab2 test, (S)B3hA, (S)B3hA, (S)B3hA, (S)B3hA, (S)B3hV, (S)B3hA, (S)B3hA

    fold_bp [ H14M H14M H14M H14M H14M H14M H14M], test

    fold_bp [ H14M (-140.3 66.5 136.8) H14M H14M H14M (-140.3 66.5 136.8) H14M], test # the same as above

Note that not every peptide sequence supports every secondary structure. For example, you will find steric clashes if
you try to fold an (*R*)-homochiral β\ :superscript:`3`-peptide into the H14M helix:

.. code::

    betafab2 invalid, (R)B3hL, (R)B3hL, (R)B3hL, (R)B3hL, (R)B3hL

    fold_bp H14M, invalid

In other cases when the sequence and the desired fold are compatible, you can still get steric clashes because the
folding procedure does not touch the side-chains. Luckily these can easily be removed by the sculpting facility of
PyMOL. For example, when the above peptide is produced in the default, extended conformation, there is a steric clash
between the leucine side-chains and the amide oxygen. To resolve them, first fix the backbone atoms and initialize
sculpting.

.. code::

    betafab2 clash, (R)B3hL, (R)B3hL, (R)B3hL, (R)B3hL, (R)B3hL

    # select the backbone -> makes a new selection (bbone)
    select_bbb bbone, invalid

    # fix the backbone atoms
    flag fix, (bbone), set

    # Initialize sculpting
    sculpt_activate invalid

    # Do some iterations
    sculpt_iterate invalid, cycles=1000

    # Deactivate sculpting
    sculpt_deactivate invalid

    # make the backbone atoms free
    flag fix, (bbone), clear

Or if your PyMOL installation "includes modelling capabilities" [1]_, you can get even better results with the `clean`
command:

.. code::

    betafab2 clash, (R)B3hL, (R)B3hL, (R)B3hL, (R)B3hL, (R)B3hL

    # select the backbone -> makes a new selection (bbone)
    select_bbb bbone, invalid

    # fix the backbone atoms
    flag fix, (bbone), set

    # Geometry optimization
    clean invalid

    # make the backbone atoms free
    flag fix, (bbone), clear

Graphical user interface
------------------------

This plug-in supplies two GUI tools, available in the Plugin menu of the PyMOL main menu bar.

.. toctree::
    betafabgui
    sseditor

.. [1] In other words, if the `freemol` extension is installed. It is installed by default in the Incentive and the
    Education version of PyMOL, but it can also be made to work in the open source version.