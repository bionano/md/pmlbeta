.. pmlbeta documentation master file, created by
   sphinx-quickstart on Sun Feb  3 21:08:31 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pmlbeta's documentation!
===================================


`pmlbeta` is an extension for `PyMOL <https://pymol.org>`_, containing utilities and algorithms for dealing with
β-peptides. It has facilities for creating and manipulating geometries of natural α- and artificial β- and even mixed
α/β-peptides.

.. image:: _static/ttk_logo.png
   :align: right
   :alt: TTK LOGO
   :width: 64px
   :target: https://bionano.ttk.hu

This program has been developed by the `BioNano Platform <https://bionano.ttk.hu>`_ of the `Hungarian Research Centre for
Natural Sciences <https://www.ttk.hu>`_

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   installation
   quickstart
   residuenames
   secstruct_db
   recognition
   autodoc/index
   citing
   bugreport

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
