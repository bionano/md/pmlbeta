### This PyMOL scripts builds six peptides to showcase the possibilities of betafab2.

# Build a beta-homoglycine dimer
betafab2 2xhGly, BA, BA
color green, e. C
cmd.translate([-coord for coord in cmd.centerofmass('2xhGly')], '2xhGly')
save 2xhGly.pse, 2xhGly

# build a single beta-amino acid: (S)-Beta2-homoValine
betafab2 SB2hVal, (S)B2hV
color green, e. C
super SB2hVal, 2xhGly
cmd.translate([-coord for coord in cmd.centerofmass('SB2hVal')], 'SB2hVal')
save SB2hVal.pse, SB2hVal

# build the "VALXVAL" beta-peptide in extended conformation
betafab2 Ac_VALXVAL_NMe_straight, ACE, (S)B3hV, (S)B3hA, (S)B3hL, (2S3S)B23h(2A3A), (S)B3hV, (S)B3hA, (S)B3hL, NME
color green, e. C
super Ac_VALXVAL_NMe_straight, 2xhGly
cmd.translate([-coord for coord in cmd.centerofmass('Ac_VALXVAL_NMe_straight')], 'Ac_VALXVAL_NMe_straight')
save Ac_VALXVAL_NMe_straight.pse, Ac_VALXVAL_NMe_straight

# build the "VALXVAL" beta-peptide in H14M helix
betafab2 Ac_VALXVAL_NMe_helix, ACE, (S)B3hV{H14M}, (S)B3hA{H14M}, (S)B3hL{H14M}, (2S3S)B23h(2A3A){H14M}, (S)B3hV{H14M}, (S)B3hA{H14M}, (S)B3hL{H14M}, NME
color green, e. C
super Ac_VALXVAL_NMe_helix, 2xhGly
cmd.translate([-coord for coord in cmd.centerofmass('Ac_VALXVAL_NMe_helix')], 'Ac_VALXVAL_NMe_helix')
save Ac_VALXVAL_NMe_helix.pse, Ac_VALXVAL_NMe_helix

# build a mixed beta-peptide from B2, B3, B23 and cyclic residues
betafab2 mixed_B2_B3_B23_cyclic, (S)B2hV, (S)B3hA, (S)B3hL, (2S3R)B23h(2A3A), (2R3S)ACPC, (S)B3hV, (S)B3hA, (2S3S)ACHC, (S)B3hL
color green, e. C
super mixed_B2_B3_B23_cyclic, 2xhGly
cmd.translate([-coord for coord in cmd.centerofmass('mixed_B2_B3_B23_cyclic')], 'mixed_B2_B3_B23_cyclic')
save mixed_B2_B3_B23_cyclic.pse, mixed_B2_B3_B23_cyclic

# build a mixed peptide of alpha- and beta-amino acids
betafab2 mixed_AB, (S)B3hV, (S)B2hQ, (2S3R)B23h(2A3A), BA, (S)AW, (R)AY, (2S3R)B23h(2A3A)
color green, e. C
super mixed_AB, 2xhGly
cmd.translate([-coord for coord in cmd.centerofmass('mixed_AB')], 'mixed_AB')
save mixed_AB.pse, mixed_AB


# arrange the peptides in a grid and make a PNG image
set grid_mode, 1

set grid_slot, 1, 2xhGly
set grid_slot, 2, SB2hVal
set grid_slot, 3, Ac_VALXVAL_NMe_straight
set grid_slot, 4, Ac_VALXVAL_NMe_helix
set grid_slot, 5, mixed_B2_B3_B23_cyclic
set grid_slot, 6, mixed_AB

center
viewport  800,  600
set_view (\
     0.764963865,   -0.528781056,   -0.367723227,\
     0.155217990,    0.705459177,   -0.691542983,\
     0.625089705,    0.471928865,    0.621727943,\
     0.000000000,    0.000000000, -100.462196350,\
    -0.202455521,   -0.029425621,    0.086241722,\
    81.547805786,  119.376556396,  -20.000000000 )

ray
png betafab2test.png, width=10cm, dpi=600, ray=1
save betafab2test.pse