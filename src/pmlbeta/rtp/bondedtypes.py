class BondedTypes:
    def __init__(self, bondfcn, anglefcn, dihedralfcn, improperfcn, alldihedrals=False, nrexcl=3, hh14=True,
                 removedih=False):
        self.bondfunction = bondfcn
        self.anglefunction = anglefcn
        self.dihedralfunction = dihedralfcn
        self.improperfunction = improperfcn
        self.keepalldihedrals = bool(alldihedrals)
        self.nrexcl = nrexcl
        self.hh14 = bool(hh14)
        self.removedihedralsifwithimproper = bool(removedih)
