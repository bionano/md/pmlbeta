from PyQt5 import QtWidgets, QtCore
from pymol import cmd, CmdException

from .recognition_ui import Ui_Form
from .recognition import residue_topology_candidates
from .residuetopologychoicesmodel import ResidueTopologyChoicesModel
from ..betafabgui2.comboboxdelegate import ComboBoxDelegate
from ..rtp import RTPEntry


class RTPMatchUI(QtWidgets.QWidget, Ui_Form):
    """Graphical user interface for matching residue topologies"""
    def __init__(self, parent=None):
        super().__init__(parent)
        self._model = ResidueTopologyChoicesModel()
        self._residuechoicedelegate = None
        self._rtpentries = []
        self.setupUi(self)

    def setupUi(self, Form):
        """Initialize the UI"""
        super().setupUi(Form)
        self._residuechoicedelegate = ComboBoxDelegate(self.choicesTreeView, kind=QtCore.Qt.UserRole)
        self.choicesTreeView.setItemDelegateForColumn(1, self._residuechoicedelegate)
        self.choicesTreeView.setModel(self._model)
        self.browseRTPToolButton.clicked.connect(self.browseRTP)
        self.reloadModelsToolButton.clicked.connect(self.reloadModels)
        self.modelComboBox.currentTextChanged.connect(self.modelChanged)
        self.rtpLineEdit.editingFinished.connect(self.rtpChanged)
        self.findMatchingPushButton.clicked.connect(self.findMatching)
        self.AssignPushButton.clicked.connect(self.assignRTP)
        self.reloadModels()

    def browseRTP(self):
        """Open a window for selecting a .rtp file."""
        filename, filter_ = QtWidgets.QFileDialog.getOpenFileName(
            self, 'Open GROMACS residue topology database file', '',
            'GROMACS residue databases (*.rtp);; All files (*)',
            'GROMACS residue databases (*.rtp)')
        if not filename:
            return
        self.rtpLineEdit.setText(filename)
        self.rtpLineEdit.editingFinished.emit()

    def reloadModels(self):
        """Fetch models and selections defined in PyMOL and add them to the combo box"""
        self.modelComboBox.clear()
        self.modelComboBox.addItems(cmd.get_names('public_objects'))
        self.modelComboBox.addItems(['({})'.format(sele) for sele in cmd.get_names('public_selections')])

    def modelChanged(self):
        """Another model has been selected: update the choices treeview"""
        # first check if this is a valid selection
        self.choicesTreeView.model().modelReset()
        try:
            self.atomCountLabel.setText(str(cmd.count_atoms(self.modelComboBox.currentText())))
        except CmdException:
            self.atomCountLabel.setText('(invalid selection)')
            return
        for resi in sorted({atom.resi_number for atom in cmd.get_model(self.modelComboBox.currentText()).atom}):
            self.choicesTreeView.model().addResidue(resi, [], None)

    def rtpChanged(self):
        """The RTP file changed: reload residue topologies"""
        print('Reloading residue topologies from file {}'.format(self.rtpLineEdit.text()))
        try:
            self._rtpentries = list(RTPEntry.loadrtpfile(self.rtpLineEdit.text()))
            self.rtpCountLabel.setText(str(len(self._rtpentries)))
        except Exception as exc:
            QtWidgets.QMessageBox.critical(self, 'Error while loading RTP file', 'RTP file {} could not be loaded: {}'.format(self.rtpLineEdit.text(), str(exc)))
            self.rtpLineEdit.clear()
            self.rtpCountLabel.setText('--')
            return

    def findMatching(self):
        """Find matching residue topologies for all selected residues in this model."""
        resis = [residuedata.resi for residuedata in self._model.residues() if residuedata.active]
        print('Finding matching topologies for residues {}'.format(resis))
        candidates = residue_topology_candidates(self.modelComboBox.currentText(), self._rtpentries, resis)
        for resi, cand in zip(resis, candidates):
            self._model.updateChoices(resi, [r.name for r in cand])

    def assignRTP(self):
        """Assign atom properties from the matched residue topology"""
        for residuedata in self._model.residues():
            if not residuedata.active:
                continue
            rtpentry = [rtp for rtp in self._rtpentries if rtp.name == residuedata.selected][0]
            rtpentry.matchResidue(self.modelComboBox.currentText(), residuedata.resi, updateifisomorphic=True)


rtpmatchingwidget = None  # global instance


def run():
    """Create the global instance if not present and show it."""
    global rtpmatchingwidget
    if not rtpmatchingwidget:
        rtpmatchingwidget = RTPMatchUI()
    rtpmatchingwidget.show()
    rtpmatchingwidget.raise_()
