from typing import Optional, Sequence, Any

from PyQt5 import QtCore


class ResidueTopologyChoicesModel(QtCore.QAbstractItemModel):
    class DataItem:
        def __init__(self, resi: int, choices: Sequence[str], selected: Optional[str] = None):
            self.resi = resi
            self.choices = list(choices)
            self.selected = selected
            self.active = True

    def __init__(self):
        super().__init__()
        self._data = []

    def data(self, index: QtCore.QModelIndex, role: int = QtCore.Qt.DisplayRole) -> Any:
        if (index.column() == 0) and (role == QtCore.Qt.DisplayRole):
            return str(self._data[index.row()].resi)
        elif (index.column() == 1) and (role == QtCore.Qt.DisplayRole):
            return self._data[index.row()].selected if self._data[index.row()].selected is not None else '--'
        elif (index.column() == 1) and (role == QtCore.Qt.EditRole):
            return self._data[index.row()].selected
        elif (index.column() == 1) and (role == QtCore.Qt.UserRole):
            return self._data[index.row()].choices
        elif (index.column() == 0) and (role == QtCore.Qt.CheckStateRole):
            return [QtCore.Qt.Unchecked, QtCore.Qt.Checked][self._data[index.row()].active]
        else:
            return None

    def setData(self, index: QtCore.QModelIndex, value: Any, role: int = ...) -> bool:
        if (index.column() == 1) and (role == QtCore.Qt.EditRole):
            if value in self._data[index.row()].choices:
                self._data[index.row()].selected = value
                return True
            else:
                raise ValueError('Value {} is not among the valid choices for residue {}'.format(value, index.row()))
        elif index.row() == 0 and role == QtCore.Qt.CheckStateRole:
            self._data[index.row()].active = value==QtCore.Qt.Checked
            return True
        return False

    def columnCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return 2

    def rowCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return len(self._data)

    def index(self, row: int, column: int, parent: QtCore.QModelIndex = ...) -> QtCore.QModelIndex:
        return self.createIndex(row, column, None)

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlag:
        if index.column() == 0:
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemNeverHasChildren | QtCore.Qt.ItemIsUserCheckable
        elif index.column() == 1 and len(self._data[index.row()].choices):
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemNeverHasChildren | QtCore.Qt.ItemIsEditable
        elif index.column() == 1 and not len(self._data[index.row()].choices):
            return QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemNeverHasChildren

    def parent(self, child: QtCore.QModelIndex) -> QtCore.QModelIndex:
        return QtCore.QModelIndex()

    def headerData(self, section: int, orientation: QtCore.Qt.Orientation, role: int = ...) -> Any:
        return ['Residue', 'Topology'][section] if (
                (orientation == QtCore.Qt.Horizontal) and (role == QtCore.Qt.DisplayRole)) else None

    def modelReset(self) -> None:
        self.beginResetModel()
        self._data = []
        self.endResetModel()

    def addResidue(self, resi: int, choices: Sequence[str], selected: Optional[str] = None):
        self.beginInsertRows(QtCore.QModelIndex(), self.rowCount(), self.rowCount() + 1)
        try:
            self._data.append(self.DataItem(resi, choices, selected))
        finally:
            self.endInsertRows()

    def residues(self):
        return self._data

    def updateChoices(self, resi:int, choices: Sequence[str]):
        self.beginResetModel()
        try:
            for d in self._data:
                if d.resi == resi:
                    d.choices=list(choices)
                    if not choices:
                        d.selected=None
                    elif d.selected not in choices:
                        d.selected=choices[0]
        finally:
            self.endResetModel()

