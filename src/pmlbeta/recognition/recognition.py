from typing import Sequence, Optional, Union

import pymol.cmd as cmd

from ..rtp.rtp import RTPEntry


def renumber_peptide_chain(nterm: str, cterm: str):
    """
    DESCRIPTION

    Re-number residues consecutively in a peptide chain

    USAGE

    renumber_peptide_chain nterm, cterm

    ARGUMENTS

    nterm = single-atom selection of the N-terminal nitrogen

    cterm = single-atom selection of the C-terminal carbon

    NOTES

    information in the `ID` atom property is destroyed.
    """
    # first do some sanity checks
    # check #1: nterm and cterm must be singletons
    if cmd.count_atoms(nterm) != 1:
        raise ValueError('Argument `nterm` must be a selection containing exactly one atom.')
    if cmd.count_atoms(cterm) != 1:
        raise ValueError('Argument `cterm` must be a selection containing exactly one atom.')
    # check #2: nterm and cterm must be in the same molecule
    if cmd.count_atoms('(bymolecule ({})) or (bymolecule ({}))'.format(nterm, cterm)) != \
            cmd.count_atoms('bymolecule ({})'.format(cterm)):
        raise ValueError('The atoms in `nterm` and `cterm` must be in the same molecule.')
    # check #3: nterm must be a nitrogen atom
    if cmd.get_model(nterm).atom[0].symbol != 'N':
        raise ValueError('N-terminal atom is not a nitrogen')
    # check #4: cterm must be a carbon atom
    if cmd.get_model(cterm).atom[0].symbol != 'C':
        raise ValueError('C-terminal atom is not a carbon')

    # we trace the backbone from Nterm to Cterm and tag its atoms with consecutive residue numbers. To accomplish this,
    # we find the topological distance (i.e. number of separating bonds) of each atom from the C-terminal atom, and then
    # starting from the N-terminal atom, we walk along decreasing topological distances. Note that if the backbone
    # contains loops where the two branches are of the same length, this won't work.

    # as only incentive (paid) PyMOL has atom-based custom properties, we sacrifice the information in the ID property
    # to store the topological distances.
    molecule = '(bymolecule {})'.format(nterm)

    cmd.alter(molecule, 'ID=-1')  # -1 means "not yet set"
    cmd.alter(cterm, 'ID=0')  # The C terminus is 0 bonds away from itself
    maxdist = 0  # the largest topological distance encountered up to now.
    # flood fill the molecule.
    while (cmd.alter('{} and (neighbor ID {}) and (ID \-1)'.format(molecule, maxdist),
                     'ID={}'.format(maxdist + 1))):
        maxdist += 1
    # now every atom has its topological distance marked in the ID property.
    # start from the N-terminal. It should have only one neighbour with ID one less than itself. Zero should not happen.
    # More than one: the backbone has one or more loops with equal branches
    cmd.alter(molecule, 'resi=0')  # prime all atoms
    currentresi = 1
    cmd.select('_pointer', nterm)  # this selection will refer to the current backbone atom we are visiting.
    pointerid = cmd.get_model('_pointer').atom[0].id  # get the topological distance of the N-terminal atom.
    while pointerid > 0:
        cmd.alter('_pointer', 'resi={}'.format(currentresi))  # set the residue number of the current atom.
        cmd.select('_pointer', '(neighbor "_pointer") and (ID {})'.format(pointerid - 1))  # go to the next atom
        if cmd.count_atoms('_pointer') != 1:  # check for loops
            # if a loop is found, bail out.
            raise ValueError('Backbone contains a cycle or disconnected')
        if cmd.get_model('_pointer').atom[0].symbol == 'N':
            # if the next atom is a nitrogen, we start a new residue.
            currentresi = currentresi + 1
        pointerid -= 1  # much faster than cmd.get_model('_pointer').atom[0].id
    cmd.delete('_pointer')  # tidy up after ourselves.
    # now the atoms in the backbone have their correct `resi` set
    # Set the residue of sidechain atoms by flood-filling from the backbone.
    while True:
        for resi in range(1, currentresi + 1):
            if cmd.alter('(neighbor resi {}) and (resi 0)'.format(resi), 'resi={}'.format(resi)):
                # we did some alterations, break the for loop
                break
        else:
            # if we reach here, no alterations were done in any residue, i.e. we are ready marking the molecule.
            break


def residue_topology_candidates(molecule, rtpfile, residues: Optional[Sequence[int]] = None):
    """
    DESCRIPTION

    Get candidate residue topologies for the residues in a molecular model

    USAGE

    residue_topology_candidates molecule, rtpfilename

    ARGUMENTS

    molecule = selection containing the molecule

    rtpfilename = path to a GROMACS residue topology database file (.rtp extension)

    NOTES

    Names of matching topologies are listed.

    SEE ALSO

    assign_residue_topology

    """
    if isinstance(residues, int):
        residues = [residues]
    if isinstance(rtpfile, str):
        rtpentries = list(RTPEntry.loadrtpfile(rtpfile))
    else:
        rtpentries = rtpfile
    candidates = []
    for resi in residues:
        candidates.append([r for r in rtpentries if r.matchResidue(molecule, resi, updateifisomorphic=False)])
        print('Candidate topologies for residue {}: {}'.format(resi, ', '.join([r.name for r in candidates[-1]])))
    return candidates


def assign_residue_topology(molecule: str, rtpfilename: str, resi: Union[int, str], resn: str):
    """
    DESCRIPTION

    Assign atom names, residue names and partial charges from a GROMACS residue topology entry

    USAGE

    assign_residue_topology molecule, rtpfilename, resi, resn

    ARGUMENTS

    molecule = selection of the molecule

    rtpfilename = file name of a GROMACS residue topology database file (.rtp extension)

    resi = residue index

    resn = residue topology entry name

    NOTE

    Topological isomorphism check will always be performed.

    SEE ALSO

    residue_topology_candidates
    """
    if isinstance(resi, str):
        resi = int(resi)
    if not isinstance(resi, int):
        raise ValueError('Residue index must be an integer')
    for rtp in RTPEntry.loadrtpfile(rtpfilename):
        if rtp.name == resn:
            if not rtp.matchResidue(molecule, resi, updateifisomorphic=True):
                raise ValueError(
                    'Could not match residue {} of molecule "{}" with rtp entry {}'.format(resi, molecule, resn))
            break
    else:
        raise ValueError('Could not find residue topology entry {} in file {}'.format(resn, rtpfilename))
