# pmlbeta

A PyMOL plugin for beta peptides

## Documentation

https://pmlbeta.rtfd.io

## Installation

### From zip

Download the latest binary package from
[https://gitlab.com/awacha/pmlbeta/raw/binaries/pmlbeta_latest.zip]
and install it with the plugin manager of PyMOL.

Or you can give the above URL to PyMOL's package manager.

### By hand

Check out the repository by

```bash
git clone https://gitlab.com/awacha/pmlbeta.git
```
Create the plugin zip file with:

```bash
$ python makebundle.py
```

Then use the plugin manager of PyMOL to install the plugin from the zip
file.
